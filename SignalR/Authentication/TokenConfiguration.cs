﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Authentication
{
    public class TokenConfiguration
    {
        public string Path { get; set; } = "/token";
        public string Key { get; set; } = "cnajsbdyuhsabvdgyuvaytdvcastydctr6yCYTURCytrdxcREDXc";
        public string Issuer { get; set; } = "SignalRAuthTest";
        public string Audience { get; set; } = "SignalRAuthTest";
        public TimeSpan Expiration { get; set; } = TimeSpan.FromHours(1);
        public TimeSpan RefreshExpiration { get; set; } = TimeSpan.FromHours(5);
    }
}
