﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SignalR.Models;
using SignalR.Services;

namespace SignalR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NotificationController : ControllerBase
    {
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> EnviarNotificacao([FromBody] Notification notification, [FromServices] IMessageService _messageService)
        {
            var mensagem = new
            {
                Mensagem = notification.Mensagem,
                Imagem = notification.Imagem
            };

            var mensagemObj = JsonConvert.SerializeObject(mensagem);

            if (notification.Global)
            {
                await _messageService.EnviarMensagemTodos("Notificacao", mensagemObj, EHubs.NOTIFICACAO);
            }
            else
            {
                if(notification.IdLogin == null)
                {
                    return BadRequest("Id login necessário");
                }

                var idLogin = notification.IdLogin.Value;
                await _messageService.EnviarMensagem("Notificacao", idLogin, mensagemObj, EHubs.NOTIFICACAO);
            }

            return Ok("Notificação enviada com sucesso");
        }
    }
}