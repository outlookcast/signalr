﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SignalR.Services;

namespace SignalR.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ImportacaoController : ControllerBase
    {
        [HttpPost]
        public async Task<IActionResult> ImportarArquivos([FromServices] IUploadService uploadService)
        {
            var files = Request.Form.Files;
            var idLogin = int.Parse(this.User.Identity.Name);

            var thread = new Task(async () => await uploadService.ProcessarUpload(files, idLogin));

            thread.Start();
            
            return Ok();
        }
    }
}