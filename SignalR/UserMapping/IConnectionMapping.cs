﻿using System.Collections.Generic;

namespace SignalR.UserMapping
{
    /// <summary>
    /// Interface usada para gerenciar as conexões do SignalR
    /// Uma forma de associar o IdLogin com as conexões
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IConnectionMapping<T>
    {
        void Add(T key, string connectionId, string hub);
        IEnumerable<string> GetConnections(T key, string hub);
        void Remove(T key, string connectionId, string hub);
    }
}
