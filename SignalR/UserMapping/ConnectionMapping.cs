﻿using System.Collections.Generic;
using System.Linq;

namespace SignalR.UserMapping
{
    public class ConnectionMapping<T> : IConnectionMapping<T>
    {
        public ConnectionMapping()
        {
            _connections = new Dictionary<string, Dictionary<T, HashSet<string>>>();

            // Cria mapeamento para os usuários conectados no Hub de Importação e de Notificação de forma separada
            _connections.Add("Importacao", new Dictionary<T, HashSet<string>>());
            _connections.Add("Notificacao", new Dictionary<T, HashSet<string>>());
        }

        private readonly Dictionary<string, Dictionary<T, HashSet<string>>> _connections;

        public int Count
        {
            get
            {
                return _connections.Count;
            }
        }

        /// <summary>
        /// Método para mapear uma conexão à um usuário (Id Lodin)
        /// Cada aba que o usuário abrir no Chrome vai ser uma nova conexão que o
        /// browser dele abre com o backend
        /// </summary>
        /// <param name="key">Id login do usuário</param>
        /// <param name="connectionId">Connection Id do SignalR</param>
        /// <param name="hub">Hub do usuário</param>
        public void Add(T key, string connectionId, string hub)
        {
            var connectionsHub = _connections[hub];

            lock (_connections)
            {
                lock (connectionsHub)
                {
                    HashSet<string> connections;
                    if (!connectionsHub.TryGetValue(key, out connections))
                    {
                        connections = new HashSet<string>();
                        connectionsHub.Add(key, connections);
                    }

                    lock (connections)
                    {
                        connections.Add(connectionId);
                    }
                }
            }
        }

        /// <summary>
        /// Método para retornar as ConnectionIds (SignalR) 
        /// à partir do Id Login do cliente e do Hub
        /// </summary>
        /// <param name="key">Id Login do usuário</param>
        /// <param name="hub">Hub do usuário</param>
        /// <returns></returns>
        public IEnumerable<string> GetConnections(T key, string hub)
        {
            var connectionsHub = _connections[hub];

            HashSet<string> connections;
            if (connectionsHub.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        /// <summary>
        /// Função para remover determinada ConnectionId (SignalR)
        /// de um usuário (Id Login)
        /// Método chamado quando cliente fecha o browser, sendo assim, não
        /// há necessidade de enviar mensagens pois a conexão foi fechada
        /// </summary>
        /// <param name="key"></param>
        /// <param name="connectionId"></param>
        /// <param name="hub"></param>
        public void Remove(T key, string connectionId, string hub)
        {
            var connectionsHub = _connections[hub];

            lock (_connections)
            {
                lock (connectionsHub)
                {
                    HashSet<string> connections;
                    if (!connectionsHub.TryGetValue(key, out connections))
                    {
                        return;
                    }

                    lock (connections)
                    {
                        connections.Remove(connectionId);

                        if (connections.Count == 0)
                        {
                            connectionsHub.Remove(key);
                        }
                    }
                }
            }
        }
    }
}
