﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Models
{
    /// <summary>
    /// Modelo de mensagem que irei enviar para o client
    /// através do SignalR
    /// </summary>
    public class UploadMessage
    {
        public Guid IdImportacao { get; set; }
        public DateTime Horario { get; set; }
        public decimal PorcentagemConclusao { get; set; }
        public string MensagemLog { get; set; }
    }
}
