﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Models
{
    public class Notification
    {
        [Required]
        public bool Global { get; set; }

        public int? IdLogin { get; set; }

        [Required]
        public string Imagem { get; set; }

        [Required]
        public string Mensagem { get; set; }
    }
}
