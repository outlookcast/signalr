﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Services
{
    public interface IUploadService
    {
        Task ProcessarUpload(IFormFileCollection arquivos, int idLogin);
    }
}
