﻿using SignalR.Models;

namespace SignalR.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
    }
}
