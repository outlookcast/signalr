﻿using Microsoft.AspNetCore.Http;
using SignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SignalR.Services
{
    public class UploadService : IUploadService
    {
        private readonly IMessageService _messageService;
        private readonly EHubs _hub;
        private readonly string _topicName = "MensagensImportacao";

        public UploadService(IMessageService messageService)
        {
            _messageService = messageService;
            _hub = EHubs.IMPORTACAO;
        }

        /// <summary>
        /// Função fake responsável pelo upload dos arquivos
        /// </summary>
        /// <param name="arquivos"></param>
        /// <returns></returns>
        public async Task ProcessarUpload(IFormFileCollection arquivos, int idLogin)
        {
            var idImportacao = Guid.NewGuid();
            var porcentagem = 0m;

            var mensagem = new UploadMessage
            {
                IdImportacao = idImportacao,
                Horario = DateTime.Now,
                MensagemLog = "Iniciando importação",
                PorcentagemConclusao = porcentagem
            };

            await _messageService.EnviarMensagem(mensagem, idLogin, _topicName, _hub);

            var numArquivos = arquivos.Count;
            var cont = 0;

            foreach (var arquivo in arquivos)
            {
                Thread.Sleep(1000 * 1);

                cont++;

                decimal porcentagemAux = ((decimal)cont / (decimal)numArquivos) * 100m;
                porcentagem = Math.Round(porcentagemAux, 2);

                mensagem = new UploadMessage
                {
                    IdImportacao = idImportacao,
                    Horario = DateTime.Now,
                    MensagemLog = $"Importando arquivo {arquivo.FileName}",
                    PorcentagemConclusao = porcentagem == 100m ? 99m : porcentagem
                };

                await _messageService.EnviarMensagem(mensagem, idLogin, _topicName, _hub);
            }

            Thread.Sleep(1000 * 1);

            mensagem = new UploadMessage
            {
                IdImportacao = idImportacao,
                Horario = DateTime.Now,
                MensagemLog = $"Importacao concluída com sucesso",
                PorcentagemConclusao = 100m
            };

            await _messageService.EnviarMensagem(mensagem, idLogin, _topicName, _hub);
        }
    }
}
