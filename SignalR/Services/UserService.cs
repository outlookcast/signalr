﻿using Microsoft.Extensions.Configuration;
using SignalR.Authentication;
using SignalR.Models;
using System;

namespace SignalR.Services
{
    public class UserService : IUserService
    {
        private readonly TokenConfiguration _tokenConfiguration;

        public UserService(IConfiguration configuration, TokenConfiguration tokenConfiguration)
        {
            _tokenConfiguration = tokenConfiguration;
        }

        /// <summary>
        /// Função fake para fazer autenticação
        /// </summary>
        /// <param name="username">Nome do usuário</param>
        /// <param name="password">Senha do usuário</param>
        /// <returns></returns>
        public User Authenticate(string username, string password)
        {
            var user = new User
            {
                Id = 1,
                FirstName = "First Name",
                LastName = "Last Name",
                Username = username,
                Password = password
            };

            user.Token = JwtAuthorization.GenerateToken(user.Id.ToString(), _tokenConfiguration);

            return user;
        }
    }
}
