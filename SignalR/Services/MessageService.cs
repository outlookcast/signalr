﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SignalR.Hubs;
using SignalR.Models;
using SignalR.UserMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Services
{
    public class MessageService : IMessageService
    {
        private readonly IConfiguration _configuration;
        private readonly IHubContext<ImportacaoHub> _importacaoHub;
        private readonly IHubContext<NotificacaoHub> _notificacaoHub;
        private readonly IConnectionMapping<int> _connectionMapping;

        public MessageService(IConfiguration configuration, IHubContext<ImportacaoHub> importacaoHub, IHubContext<NotificacaoHub> notificacaoHub, IConnectionMapping<int> connectionMapping)
        {
            _configuration = configuration;
            _importacaoHub = importacaoHub;
            _notificacaoHub = notificacaoHub;
            _connectionMapping = connectionMapping;
        }

        private string GetHubName(EHubs hub)
        {
            switch (hub)
            {
                case EHubs.IMPORTACAO:
                    return "Importacao";
                case EHubs.NOTIFICACAO:
                    return "Importacao";
                default:
                    return null;
            }
        }

        /// <summary>
        /// Método para enviar mensagem para um clientes específico 
        /// </summary>
        /// <param name="mensagemObj">Objeto para enviar para o client</param>
        /// <param name="idLogin">Id Login do destinatário</param>
        /// <param name="topic">Tópico no qual deseja enviar a mensagem</param>
        /// <param name="hub">Hub que deseja enviar a mensagem</param>
        /// <returns></returns>
        public async Task EnviarMensagem(object mensagemObj, int idLogin, string topic, EHubs hub)
        {
            var mensagem = JsonConvert.SerializeObject(mensagemObj);

            var hubName = GetHubName(hub);
            IReadOnlyList<string> connections = _connectionMapping.GetConnections(idLogin, hubName).ToList();

            switch (hub)
            {
                case EHubs.IMPORTACAO:
                    await _importacaoHub.Clients.Clients(connections).SendAsync(topic, mensagem);
                    break;
                case EHubs.NOTIFICACAO:
                    await _notificacaoHub.Clients.Clients(connections).SendAsync(topic, mensagem);
                    break;
            }
        }

        /// <summary>
        /// Envia a mensagem para todos os clientes do Hub
        /// </summary>
        /// <param name="topic">Tópico no qual deseja enviar a mensagem</param>
        /// <param name="mensagemObj">Objeto para enviar para o client</param>
        /// <param name="hub">Hub que deseja enviar a mensagem</param>
        /// <returns></returns>
        public async Task EnviarMensagemTodos(string topic, object mensagemObj, EHubs hub)
        {
            var mensagem = JsonConvert.SerializeObject(mensagemObj);

            switch (hub)
            {
                case EHubs.IMPORTACAO:
                    await _importacaoHub.Clients.All.SendAsync(topic, mensagem);
                    break;
                case EHubs.NOTIFICACAO:
                    await _notificacaoHub.Clients.All.SendAsync(topic, mensagem);
                    break;
            }
        }
    }
}
