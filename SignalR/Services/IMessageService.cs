﻿using SignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Services
{
    public interface IMessageService
    {
        Task EnviarMensagem(object mensagem, int idLogin, string topic, EHubs hub);
        Task EnviarMensagemTodos(string topic, object mensagem, EHubs hub);
    }
}
