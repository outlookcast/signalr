﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SignalR.Authentication;
using SignalR.Hubs;
using SignalR.Services;
using SignalR.UserMapping;

namespace SignalR
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSingleton(Configuration);

            IConnectionMapping<int> userMapping = new ConnectionMapping<int>();
            services.AddSingleton(userMapping);
            services.AddSingleton<TokenConfiguration>();
            services.AddTokenConfiguration(services.BuildServiceProvider().GetRequiredService<TokenConfiguration>());
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUploadService, UploadService>();
            services.AddCors();
            services.AddSignalR()
                .AddHubOptions<ImportacaoHub>(options => options.EnableDetailedErrors = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.WithOrigins("http://localhost", "http://localhost:3000");
                builder.AllowCredentials();
            });

            var wsOptions = new WebSocketOptions();
            wsOptions.AllowedOrigins.Add("http://localhost:3000");
            wsOptions.AllowedOrigins.Add("http://localhost");
            app.UseWebSockets(wsOptions);

            app.UseSignalR(hrb => hrb.MapHub<ImportacaoHub>("/hubs/importacao"));
            app.UseSignalR(hrb => hrb.MapHub<NotificacaoHub>("/hubs/notificacao"));

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
