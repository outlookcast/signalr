﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using SignalR.UserMapping;
using System;
using System.Threading.Tasks;

namespace SignalR.Hubs
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ImportacaoHub : Hub
    {
        private readonly IConnectionMapping<int> _connectionMapping;
        private readonly string _hubName;

        public ImportacaoHub(IConnectionMapping<int> connectionMapping)
        {
            _connectionMapping = connectionMapping;
            _hubName = "Importacao";
        }

        public override async Task OnConnectedAsync()
        {
            var userId = int.Parse(Context.User.Identity.Name);
            var connectionId = Context.ConnectionId;

            _connectionMapping.Add(userId, connectionId, _hubName);

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            var userId = int.Parse(Context.User.Identity.Name);
            var connectionId = Context.ConnectionId;

            _connectionMapping.Remove(userId, connectionId, _hubName);

            await base.OnDisconnectedAsync(ex);
        }
    }
}
